const assert = require('chai');
const assertHttp = require('chai-http');
const app = require('../server.js');
assert.should();
assert.use(assertHttp);


describe('Testing Documentation', () => {
    describe('GET /api', () => {
        it('It should return status 200', (done) => {
            assert.request(app)
                  .get("/api")
                  .end((err, response) => {
                      response.should.have.status(200);
                  done();
                  });
        });
        it('It should NOT return status 200', (done) => {
            assert.request(app)
                  .get("/apii")
                  .end((err, response) => {
                      response.should.have.status(404);
                  done();
                  });
        });
    });
});


describe('Testing /login', () => {
    describe('GET /login', () => {
        it('It should return status 200', (done) => {
            assert.request(app)
                  .get('/login')
                  .end((err, res) => {
                      res.should.have.status(200);
                  done();
                  });
        });
    });

    describe('POST /login', () => {
        it('It should return 200 if credentials are correct', (done) => {
            assert.request(app)
                  .post('/login')
                  .send({'username': 'marko', 'password': 'marko'})
                  .end((err, res) => {
                      res.should.have.status(200);
                  done();
                  });
        });

        it('It should return 403 if credentials are incorrect', (done) => {
            assert.request(app)
                  .post('/login')
                  .send({'username': 'f', 'password': 'f'})
                  .end((err, res) => {
                      res.should.have.status(403);
                  done();
                  });
        });
        
    });

    describe('GET /register', () => {
        it('It should return status 200', (done) => {
            assert.request(app)
                  .get('/register')
                  .end((err, res) => {
                      res.should.have.status(200);
                  done();
                  });
        });
    });

    describe('POST /register', () => {
        it('It should return 200 if user is successfully registerd', (done) => {
            assert.request(app)
                  .post('/register') //!!!izbrisat ako postoji
                  .send({'username': 'test01', 'password': 'test01'})
                  .end((err, res) => {
                      res.should.have.status(200);
                  done();
                  });
        });

        it('It should return 409 if username allready exists', (done) => {
            assert.request(app)
                  .post('/register')
                  .send({'username': 'filip', 'password': 'filip'})
                  .end((err, res) => {
                      res.should.have.status(409);
                  done();
                  });
        });
        
    });
});


describe('Testing /posts', () => {
    describe('GET /posts', () => {
        it('it should return 200 if user is logged in', (done) => {
            assert.request(app)
                  .get('/posts')
                  .set('Cookie', 'jwt=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MTYsInVzZXJuYW1lIjoiZmlsaXAiLCJpYXQiOjE2MzkxMDg2MDJ9.kuWuv9HQOFDfbQoS7tsWl9fUrC0ZyEWbGbtO5U4NsTk')
                  .end((err, res) => {
                      res.should.have.status(200); 
                      done(); 
                  });
        });
    });

    describe('POST /posts', () => {
        it('it should return 200 if user is logged in', (done) => {
            assert.request(app)
                  .post('/posts')
                  .set('Cookie', 'jwt=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MTYsInVzZXJuYW1lIjoiZmlsaXAiLCJpYXQiOjE2MzkxMDg2MDJ9.kuWuv9HQOFDfbQoS7tsWl9fUrC0ZyEWbGbtO5U4NsTk')
                  .send({'postContent': 'Testiranje mocha'})
                  .end((err, res) => {
                      res.should.have.status(200); 
                      done(); 
                  });
        });
    });
    
});


describe('Testing /users/:id', () => {
    describe('GET /users/:id', () => {
        it('it should return 200 if user is logged in', (done) => {
            assert.request(app)
                  .get('/users/24') //filip if 24
                  .set('Cookie', 'jwt=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MjQsInVzZXJuYW1lIjoiZmlsaXAiLCJpYXQiOjE2MzkxMTgzOTF9.EarufXRlk5Aed1W_ZTVxIlVz6w3QGb8qXAui_rSo9Dk')
                  .end((err, res) => {
                      res.should.have.status(200); 
                      done(); 
                  });
        });
    });

    describe('GET /users/:id', () => {
        it('it should return 403 if user is trying to access another users account', (done) => {
            assert.request(app)
                  .get('/users/17') //marko id:17
                  .set('Cookie', 'jwt=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MTYsInVzZXJuYW1lIjoiZmlsaXAiLCJpYXQiOjE2MzkxMDg2MDJ9.kuWuv9HQOFDfbQoS7tsWl9fUrC0ZyEWbGbtO5U4NsTk')
                  .end((err, res) => {
                      res.should.have.status(403); 
                      done(); 
                  });
        });
    });

    describe('PUT /users/:id', () => {
        it('it should return 200 if password is changed', (done) => {
            assert.request(app)
                  .put('/users/24') //filip id 24
                  .set('Cookie', 'jwt=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MjQsInVzZXJuYW1lIjoiZmlsaXAiLCJpYXQiOjE2MzkxMTgzOTF9.EarufXRlk5Aed1W_ZTVxIlVz6w3QGb8qXAui_rSo9Dk')
                  .send({'value': 'filip'})
                  .end((err, res) => {
                      res.should.have.status(200); 
                      done(); 
                  });
        });
    });

    describe('DELETE /users/:id', () => {
        it('it should return 200 if account is deleted', (done) => {
            assert.request(app)
                  .delete('/users/44') //!!!promijenit na zadnjeg registriranog id
                  .set('Cookie', 'jwt=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MzIsInVzZXJuYW1lIjoidGVzdDAxIiwiaWF0IjoxNjM5MTE5OTg0fQ.2tSY4YZb1ZFXVFymW7rAQsPKVjcbRJb4Bdw1hfKDtmk')
                  .end((err, res) => {
                      res.should.have.status(200); 
                      done(); 
                  });
        });
    });

    
    
});


describe('Testing /users/:id/user-posts', () => {
    describe('GET /users/:id/user-posts', () => {
        it('it should return 200 if posts are retrieved', (done) => {
            assert.request(app)
                  .get('/users/24/user-posts')
                  .set('Cookie', 'jwt=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MjQsInVzZXJuYW1lIjoiZmlsaXAiLCJpYXQiOjE2MzkxMTgzOTF9.EarufXRlk5Aed1W_ZTVxIlVz6w3QGb8qXAui_rSo9Dk')
                  .end((err, res) => {
                      res.should.have.status(200);
                      done();
                  });
        });
    });
});


describe('Testing /users/:id/user-posts/:postId', () => {
    describe('GET /users/:id/user-posts/:postId', () => {
        it('it should return 200 if posts are retrieved', (done) => {
            assert.request(app)
                  .get('/users/24/user-posts/70')
                  .set('Cookie', 'jwt=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MjQsInVzZXJuYW1lIjoiZmlsaXAiLCJpYXQiOjE2MzkxMTgzOTF9.EarufXRlk5Aed1W_ZTVxIlVz6w3QGb8qXAui_rSo9Dk')
                  .end((err, res) => {
                      res.should.have.status(200);
                      done();
                  });
        });
    });

    describe('PUT /users/:id/user-posts/:postId', () => {
        it('it should return 200 if post is edited', (done) => {
            assert.request(app)
                  .put('/users/24/user-posts/70')
                  .set('Cookie', 'jwt=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MjQsInVzZXJuYW1lIjoiZmlsaXAiLCJpYXQiOjE2MzkxMTgzOTF9.EarufXRlk5Aed1W_ZTVxIlVz6w3QGb8qXAui_rSo9Dk')
                  .send({'value': 'Post za testiranje1'})
                  .end((err, res) => {
                      res.should.have.status(200);
                      done();
                  });
        });
    });

    describe('DELETE /users/:id/user-posts/:postId', () => {
        it('it should return 200 if post is deleted', (done) => {
            assert.request(app)
                  .delete('/users/24/user-posts/86') //!!!promijenit id na zadnji napisani post
                  .set('Cookie', 'jwt=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MjQsInVzZXJuYW1lIjoiZmlsaXAiLCJpYXQiOjE2MzkxMTgzOTF9.EarufXRlk5Aed1W_ZTVxIlVz6w3QGb8qXAui_rSo9Dk')
                  .end((err, res) => {
                      res.should.have.status(200);
                      done();
                  });
        });
    });

    
});