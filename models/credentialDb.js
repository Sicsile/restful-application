var sqlite3 = require('sqlite3').verbose();

const DBSOURCE = "db.sqlite";

let db = new sqlite3.Database(DBSOURCE, (err) => {
    if(err){
        console.log(err.message);
        throw err;
    } else {
        // console.log("Connected to the SQLite database");
        //users
        db.run(`create table users (
            id integer primary key autoincrement,
            username text unique,
            password text
        )`, (err) => {
            if(err){
                // console.log(err.message);
            } else{
                var insert = 'insert into users (username, password) values (?,?)';
                db.run(insert, ["filip", "filip"]);
                db.run(insert, ["marko", "marko"]);
                db.run(insert, ["ivan", "ivan"]);
                db.run(insert, ["josip", "josip"]);
                db.run(insert, ["petar", "petar"]);
            }
        });

        //posts
        db.run(`create table posts (
            postId integer primary key autoincrement,
            username text,
            postContent text
        )`, (err) => {
            if(err){
                // console.log(err.message);
            } else{
                var insert = 'insert into posts (username, postContent) values (?,?)';
                db.run(insert, ["filip", "Zlatni konci litnje zore"]);
                db.run(insert, ["marko", "Došli su u njene dvore"]);
                db.run(insert, ["ivan", "Da bi moju ljubav budili"]);
                db.run(insert, ["josip", "Svitlo nek joj jubi lice"]);
                db.run(insert, ["petar", "Lipo ka' u cesarice"]);
                db.run(insert, ["filip", "Kad je ja ne mogu jubiti"]);
                db.run(insert, ["marko", "Zlatna mriža njenog tila"]);   
            }
        });

        
    }
});

module.exports = db;