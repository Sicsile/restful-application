var express = require('express');
var router = express.Router();
var db = require("../models/credentialDb.js");
const jwt = require("jsonwebtoken");

router.get('/', (req, res) => {
    token = jwt.verify(req.cookies.jwt, process.env.TOKEN_KEY);
    let id = token.id;
    res.redirect(`/users/${id}`);
});

router.get('/:id', (req, res) => {
    db.all("select * from users", [], (err, rows) => {
        if (err) {
            res.send(err);
        } else {
            console.log(rows);
        }
    })
    token = jwt.verify(req.cookies.jwt, process.env.TOKEN_KEY);
    let id = req.params.id;
    if (id != token.id) {
        res.send(403);
    } else {
        var sql = `select * from users where id = ?`;
        var params = [id];
        db.get(sql, params, (err, rows) => {
            if (err) {
                res.send(err);
            } else {
                if (rows === undefined) {

                } else {
                    res.status(200);
                    res.render('user', {
                        username: rows.username,
                        id: rows.id,
                        password: rows.password
                    });
                }
            }
        })
    }
});

router.get('/:id/user-posts', (req, res) => {
    token = jwt.verify(req.cookies.jwt, process.env.TOKEN_KEY);
    let username = token.username;
    let id = req.params.id;
    if (id != token.id) {
        res.send(403);
    } else {
        var sql = `select * from posts where username = ?`
        var params = [username];
        db.all(sql, params, (err, rows) => {
            if (err) {
                res.send(err);
            } else {
                res.render('user-posts', {
                    rows,
                    username,
                    id
                });
            }
        });
    }
});

router.delete('/:id', (req, res) => {
    token = jwt.verify(req.cookies.jwt, process.env.TOKEN_KEY);
    let username = token.username;
    let id = req.params.id;
    //brisanje korisnika
    var sql = `delete from users where id = ?`;
    var params = [id];
    db.run(sql, params, (err, result) => {
        if (err) {
            res.send(err);
        } else {
            //brisanje komentara korisnika
            sql = `delete from posts where username = ?`;
            params = [username];
            db.run(sql, params, (err, result) => {
                if (err) {
                    res.send(err);
                } else {
                    res.redirect('http://127.0.0.1:8000/login');
                }
            });
        }
    });

});

router.put('/:id', (req, res) => {
    token = jwt.verify(req.cookies.jwt, process.env.TOKEN_KEY);
    let id = req.params.id;
    const data = req.body;
    const newPass = data.value;
    var sql = `update users set password = COALESCE(?, password) where id = ?`;
    var params = [newPass, id];
    db.run(sql, params, (err, result) => {
        if (err) {
            res.send(err);
        } else {
            res.redirect('http://127.0.0.1:8000/login');
        }
    });

});

router.get('/:id/user-posts/:postId', (req, res) => {
    token = jwt.verify(req.cookies.jwt, process.env.TOKEN_KEY);
    let id = req.params.id;
    let postId = req.params.postId;
    var sql = `select * from posts where postId = ?`;
    var params = [postId];
    db.get(sql, params, (err, row) => {
        if (err) {
            res.send(err);
        } else {
            res.render('post', {
                content: row.postContent,
                username: row.username,
                id,
                postId
            });
        }
    });
});

router.delete('/:id/user-posts/:postId', (req, res) => {
    let postId = req.params.postId;
    var sql = `delete from posts where postId = ?`;
    var params = [postId];
    db.run(sql, params, (err, result) => {
        if (err) {
            res.send(err);
        } else {
            res.status(200).send();
        }
    });
});

router.put('/:id/user-posts/:postId', (req, res) => {
    const data = req.body;
    const newContent = data.value
    let id = req.params.id;
    let postId = req.params.postId;
    var sql = `update posts set postContent = COALESCE(?, postContent) where postId = ?`;
    var params = [newContent, postId];
    db.run(sql, params, (err, result) => {
        if (err) {
            res.send(err);
        } else {
            res.status(200).send();
            // res.redirect(`http://127.0.0.1:8000/users/${id}/user-posts`);
            // res.status(200);
        }
    });
});


module.exports = router;