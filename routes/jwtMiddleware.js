const jwt = require("jsonwebtoken");
const dotenv = require("dotenv");
dotenv.config();


function createToken(payload) {
    return jwt.sign(payload, process.env.TOKEN_KEY);
}


module.exports = {
    createToken
}