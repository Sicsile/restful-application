var express = require('express');
var router = express.Router();
var db = require("../models/credentialDb.js");
const auth = require('./jwtMiddleware.js');


router.get('/', (req, res) => {
    res.render('home', {
        title: 'Home'
    });
});

router.get('/login', (req, res) => {
    res.cookie("jwt", "");
    res.render("login");
});

router.post('/login', (req, res) => {
    const username = req.body.username;
    const password = req.body.password;
    var sql = `select * from users where username = ? and password = ?`;
    var params = [username, password];
    db.get(sql, params, (err, rows) => {
        if (err) {
            res.status(400).json({
                "error": err.message
            });
        } else {
            if (rows === undefined) {
                res.status(403);
                res.render('login');
            } else {
                let id = rows.id;
                const payload = {
                    id,
                    username
                };
                const token = auth.createToken(payload);
                res.cookie("jwt", token);
                res.redirect(301, "/posts");
            }
        }
    });
});

router.get('/register', (req, res) => {
    res.render("register")
});

router.post('/register', (req, res) => {
    const username = req.body.username;
    const password = req.body.password;
    var insert = 'insert into users (username, password) values (?,?)';
    var params = [username, password];
    db.run(insert, params, (err, rows) => {
        if(err){
            res.status(409).send(err);
        } else {
            res.redirect('/login');
        }
    });
});



module.exports = router;