var express = require('express');
var router = express.Router();
var db = require("../models/credentialDb.js");
const auth = require('./jwtMiddleware.js');
const jwt = require("jsonwebtoken");
const dotenv = require("dotenv");
dotenv.config();


router.get('/', (req, res) => {
    if (req.cookies.jwt === "") {
        res.send(401);
    } else {
        var sql = `select postId, postContent, username from posts order by postId`;
        var params = [];
        db.all(sql, params, (err, rows) => {
            if(err){
                res.send(err);
            } else {
                res.render("posts", {
                    rows
                });
            }
        })
    }
});

router.post('/', (req, res) => {
    token = jwt.verify(req.cookies.jwt, process.env.TOKEN_KEY);
    let username = token.username;
    var sql = `insert into posts (username, postContent) values (?,?)`
    var params = [username, req.body.postContent];
    db.run(sql, params);
    res.redirect('/posts');
});

router.put('/', (req, res) => {
    
})



module.exports = router;