const express = require('express');
const app = express();
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser')
app.use(express.json());
app.use(cookieParser());
app.use(express.urlencoded({
    extended: true
}));
app.use(bodyParser.urlencoded({
    extended: true
  }));

const homeRouter = require('./routes/homeRoutes.js');
const postsRouter = require('./routes/postsRoutes.js');
const usersRouter = require('./routes/usersRoutes.js');

app.set('view engine', 'ejs');
app.use('/', homeRouter);
app.use('/posts', postsRouter);
app.use('/users', usersRouter);

app.get('/api', (req, res) => {
    res.render('api');
});

let port = 8000;
app.listen(port, () => {
    console.log(`Slusanje na http://127.0.0.1:${port}/`);
});

module.exports = app;